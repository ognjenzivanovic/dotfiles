call plug#begin('$XDG_CACHE_HOME/vim/plugged')

if(has('nvim'))
Plug 'nvim-lua/plenary.nvim'
" Plug 'nvim-lua/popup.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-telescope/telescope.nvim'

Plug 'sbdchd/neoformat'

" Plug 'TimUntersberger/neogit'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
" Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" For vsnip users.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

Plug 'simrat39/rust-tools.nvim'

Plug 'frabjous/knap'

Plug 'kyazdani42/nvim-web-devicons'
Plug 'folke/trouble.nvim'


" If you have nodejs and yarn
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
" -- All the things
" Plug 'saadparwaiz1/cmp_luasnip'

" Plug 'neovim/nvim-lspconfig'
" Plug 'hrsh7th/cmp-nvim-lsp'
" 
" Plug 'onsails/lspkind-nvim'
" Plug 'nvim-lua/lsp_extensions.nvim'
" Plug 'glepnir/lspsaga.nvim'
" Plug 'simrat39/symbols-outline.nvim'
" 
" Plug 'L3MON4D3/LuaSnip'
" Plug 'rafamadriz/friendly-snippets'


" Plug 'ThePrimeagen/git-worktree.nvim'
" Plug 'mbbill/undotree'
Plug 'ThePrimeagen/harpoon'
" Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Plug 'mfussenegger/nvim-dap'
" Plug 'rcarriga/nvim-dap-ui'
" Plug 'theHamsta/nvim-dap-virtual-text'
endif

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
Plug 'bfrg/vim-cpp-modern'
Plug 'arecarn/vim-go-to-buffer'
" Plug 'tikhomirov/vim-glsl'
" Plug 'ap/vim-css-color'
Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }

Plug 'gruvbox-community/gruvbox'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
" Plug 'kaicataldo/material.vim', { 'branch': 'main' }
" Plug 'nekonako/xresources-nvim'
"
Plug 'ThePrimeagen/vim-be-good'
call plug#end()
