local configs = require'nvim-treesitter.configs'
configs.setup {
ensure_installed = "all",
highlight = {
  enable = true, 
},
indent = {
  enable = true,
}
}

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

local lsp_installer = require("nvim-lsp-installer")
lsp_installer.on_server_ready(function(server)
  local opts = {}
  server:setup(opts)
end)
