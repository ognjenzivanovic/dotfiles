
let mapleader = " "

nnoremap <leader>m :!astyle -TS --style=linux %
nnoremap <leader>n :!Neoformat
nnoremap <C-B> :update \| !g++ % -std=c++14 -g -Wall -fsanitize=address -o bin/cpprog.out && time (bin/cpprog.out < input.txt) > output.txt <cr>

nnoremap <leader>f :Files<cr>

nnoremap <leader>h :lua require("harpoon.mark").add_file()<cr>

nnoremap <leader>j :lua require("harpoon.ui").nav_file(1) <cr>
nnoremap <leader>k :lua require("harpoon.ui").nav_file(2) <cr>
nnoremap <leader>l :lua require("harpoon.ui").nav_file(3) <cr>
nnoremap <leader>; :lua require("harpoon.ui").nav_file(4) <cr>

nnoremap <leader>d :lua require("harpoon.ui").toggle_quick_menu()<cr>
" nnoremap <leader>j :Telescope find_files hidden=true<cr>
autocmd! BufNewFile,BufRead *.vs,*.fs set ft=glsl

" command! -bang -nargs=? GFiles     call fzf#vim#gitfiles(<q-args>,
" 			\ <q-args> != '?' ? (                                                                             
" 			\ <bang>0 ? fzf#vim#with_preview('up:90%')                                     
" 			\         : fzf#vim#with_preview('down:0%', 'CTRL-/')                        
" 			\) : <bang>0                                                                 
" 			\)                     
