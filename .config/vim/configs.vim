syntax enable
filetype plugin on

set tabstop=4 
set softtabstop=4
set shiftwidth=4
set smartindent

set guitablabel=%t
set relativenumber
set nu
set clipboard=unnamed
set nohlsearch
" set foldmethod=syntax

set noswapfile
set nobackup
set undofile

set termguicolors
" set fmr={,}
" set fdm=marker

set noerrorbells
set novisualbell
set t_vb=
set tm=500
set belloff=all

" set ignorecase
" set smartcase

set wildmenu 

set hidden
set nowrap

set noshowmode
set nofoldenable

set incsearch
set scrolloff=8
set signcolumn=yes
" set colorcolumn=80

set rtp^=$XDG_CONFIG_HOME/vim
set rtp+=$XDG_CACHE_HOME/vim

set encoding=utf-8
set backspace=indent,eol,start

set re=1
set lazyredraw
set ttyfast 

" enter the current millenium
set nocompatible

set completeopt-=preview
set completeopt=menu,menuone,noselect

set runtimepath^=$XDG_CONFIG_HOME/vim
set runtimepath+=$XDG_DATA_HOME/vim
set runtimepath+=$XDG_CONFIG_HOME/vim/after

set packpath^=$XDG_DATA_HOME/vim,$XDG_CONFIG_HOME/vim
set packpath+=$XDG_CONFIG_HOME/vim/after,$XDG_DATA_HOME/vim/after

let g:netrw_home = $XDG_DATA_HOME."/vim"
call mkdir($XDG_DATA_HOME."/vim/spell", 'p')
set viewdir=$XDG_DATA_HOME/vim/view | call mkdir(&viewdir, 'p')

set backupdir=$XDG_CACHE_HOME/vim/backup | call mkdir(&backupdir, 'p')
set directory=$XDG_CACHE_HOME/vim/swap   | call mkdir(&directory, 'p')
set undodir=$XDG_CACHE_HOME/vim/undo     | call mkdir(&undodir,   'p')

if !has('nvim') | set viminfofile=$XDG_CACHE_HOME/vim/viminfo | endif

if (has('termguicolors'))
	set termguicolors
endif

if (has('nvim'))
	let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
endif


