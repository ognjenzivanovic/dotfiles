vim.opt.tabstop = 4 
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.smartindent = true

-- vim.opt.guitablabel = %t
vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.clipboard:prepend("unnamedplus")
vim.opt.clipboard:prepend("unnamed")

vim.opt.hlsearch = false
vim.opt.incsearch = true
-- vim.opt.foldmethod = syntax

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("XDG_CACHE_HOME") .. "/vim/undo"
vim.opt.undofile = true

vim.opt.termguicolors = true
-- vim.opt.fmr = {,}
-- vim.opt.fdm = marker

vim.opt.errorbells = false
vim.opt.visualbell = false
-- vim.opt.t_vb = 
vim.opt.tm = 500
vim.opt.belloff = all

-- vim.opt.ignorecase
-- vim.opt.smartcase

vim.opt.wildmenu = true

vim.opt.hidden = true
vim.opt.wrap = false

vim.opt.showmode = false
vim.opt.foldenable = false

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
-- vim.opt.colorcolumn = 80

-- vim.opt.rtp^ = $XDG_CONFIG_HOME/vim
-- vim.opt.rtp+ = $XDG_CACHE_HOME/vim

vim.opt.re = 1
vim.opt.lazyredraw = true
vim.opt.ttyfast = true

vim.opt.termguicolors = true

-- enter the current millenium
vim.opt.compatible = false


