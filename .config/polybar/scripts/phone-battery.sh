#!/bin/sh
id=$(kdeconnect-cli --list-available --id-only 2>&1)
level=$(qdbus org.kde.kdeconnect /modules/kdeconnect/devices/$id/battery org.kde.kdeconnect.device.battery.charge 2>/dev/null)
isCharging=$(qdbus org.kde.kdeconnect /modules/kdeconnect/devices/$id/battery org.kde.kdeconnect.device.battery.isCharging 2>/dev/null)
numNotifications=$(qdbus org.kde.kdeconnect /modules/kdeconnect/devices/$id/notifications org.kde.kdeconnect.device.notifications.activeNotifications 2>/dev/null)
color="#EAEDEE"
icon=""

if [[ "$id" == "0 devices found" ]]; then
	color="#FFFF00"
	icon=""
	echo -e "%{F$color}$icon%{F-}"
	exit
fi

bat10=""
bat20=""
bat30=""
bat40=""
bat50=""
bat60=""
bat70=""
bat80=""
bat90=""
bat100=""

charging=""

if [[ $level -gt 90 ]]; then 
	color="#00FF00" 
elif [[ $level -lt 20 ]]; then 
	color="#FF0000"
fi

if   [[ $level -lt 10  ]]; then icon="$bat10"
elif [[ $level -lt 20  ]]; then icon="$bat20"
elif [[ $level -lt 30  ]]; then icon="$bat30"
elif [[ $level -lt 40  ]]; then icon="$bat40"
elif [[ $level -lt 50  ]]; then icon="$bat50"
elif [[ $level -lt 60  ]]; then icon="$bat60"
elif [[ $level -lt 70  ]]; then icon="$bat70"
elif [[ $level -lt 80  ]]; then icon="$bat80"
elif [[ $level -lt 90  ]]; then icon="$bat90"
elif [[ $level -lt 100 ]]; then icon="$bat100"
fi

if [[ "$isCharging" == "true" ]]; then icon="$icon$charging"
fi

echo -e "%{F$color}$icon%{F-} %{F$color}$level%%{F-}"
