import pandas as pd
import math

# read by default 1st sheet of an excel file
df = pd.read_excel('tabela.xlsx',  header=None)

# print(dataframe1)

r, c = df.shape

# for i in range(0, 

# print (r, c)

profesori = [""] * r;
predmeti = [""] * r;

casovi = {}

for i in range(0, r):
    for j in range(0, c):
        val = str(df.at[i, j])
        isNan = (val == "nan")
        if not isNan:
            if j == 0:
                profesori[i] = val
            if j == 1:
                 profesori[i] += " "
                 profesori[i] += val;
            if j == 2:
                 predmeti[i] = val;
            if i >= 2 and j >= 3:
                 if val != "ДЕЖ.":
                    val = val.split("/")
                    val[1] = val[1].split(",")
                    val[1] = [list(el) for el in val[1]]
                    
                    for el in val[1]:
                        mod = ""
                        if len(el) == 2:
                            mod = el[1];
                        odeljenje = val[0]+"/"+el[0]
                        # print(odeljenje, i, j, mod)
                        if odeljenje in casovi:
                            casovi[odeljenje].append([i,j,mod])
                        else:  
                            casovi[odeljenje] = [[i,j,mod]]
                    
                 # v - vezbe
                 # i - izborni
                 # p - ???

raspored = {
    1: {},
    2: {},
    3: {},
    4: {},
    5: {},
}
for key, value in casovi.items():
    if key == '3/1':
        for cas in value:
            i = cas[0]
            j = cas[1]
            mod = cas[2]
            dan = math.floor((j - 2) / 14 - 0.01) + 1
            broj_casa = (j - 3) % 14 + 1
            if key[0] == '3' or key[0] == '1':
                broj_casa -= 7
                if broj_casa == 0:
                    broj_casa = -1
            profesor = profesori[i]
            predmet = predmeti[i]
            if mod == "и":
                predmet="изборни"
            elif mod != "":
                predmet = predmet + " (" + mod + ")"
            print(key, dan, mod, profesor, predmet, broj_casa)
            naziv_casa = predmet + " - " + profesor;
            if broj_casa in raspored[dan]:
                raspored[dan][broj_casa].append(naziv_casa)
            else:
                raspored[dan][broj_casa] = [naziv_casa]

for i in range(1, 6):
    for j in range(-1, 8):
        if j in raspored[i]:
            print(raspored[i][j], end=" ")
    print()
# for i in range(0, r):
#     print(profesori[i], predmeti[i])
